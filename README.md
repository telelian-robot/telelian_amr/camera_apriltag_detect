```bash
 # april tag 정보에서 april id 만 받아 pub 해주는 node
 ros2 run py_april info 

 # april id 를 sub 하여 이미지 위에 씌우고 보여주는 node
 ros2 run py_april show 
 
 # april tf 정보를 받아서 image 위에 axis draw 해주는 node 
 ros2 run py_april tf 

# info / show 동시 실행 
 ros2 launch py_april py_april.launch.py 
```

위 node를 테스트 하기 위해서는 
isaac_ros_argus_camera/ isaac_ros_apriltag package 가 필요하다

```bash 
ros2 launch isaac_ros_argus_camera isaac_ros_argus_camera_mono.launch.py

ros2 launch isaac_ros_apriltag isaac_ros_apriltag.launch.py 
```


---
## ros2 argus camera launch 파일 
### remappings 정보 
left/image_raw -> image

left/camerainfo -> camera_info
### parameters
mode - 4  # 1280* 720 으로 변경 

camera_info_url <- calibration 후 결과물 .ini 파일 경로 
안넣어 주면 april tag 실행시 에러 발생 
( april tag가 camera info 의 height width 를 넘겨 받는데 default 0으로 채워져 있어 오류가 발생함 )

## ros2 april tag launch 파일 
### parameters
size <- april tag size 기술 

max_tags <- april tag max tag 갯갯수 기술 

