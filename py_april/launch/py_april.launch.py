import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch_ros.actions import Node
from launch.launch_description_sources import PythonLaunchDescriptionSource

def generate_launch_description():
    argus_camera_node = IncludeLaunchDescription(
      PythonLaunchDescriptionSource([os.path.join(
         get_package_share_directory('isaac_ros_argus_camera'), 'launch'),
         '/isaac_ros_argus_camera_mono.launch.py'])
      )
    april_tag_node = IncludeLaunchDescription(
      PythonLaunchDescriptionSource([os.path.join(
         get_package_share_directory('isaac_ros_apriltag'), 'launch'),
         '/isaac_ros_apriltag.launch.py'])
      )
    
    return LaunchDescription([
        argus_camera_node,
        april_tag_node,
        Node(
            package='py_april',
            #namespace='april_detect_node',
            executable='info',
            name='april_detect_node'
        ),
        Node(
            package='py_april',
            #namespace='april_image_show_node',
            executable='show',
            name='april_image_show_node'
        ),
    ])