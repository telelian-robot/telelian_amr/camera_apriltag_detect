import rclpy
import numpy
import math
from rclpy.node import Node
from sensor_msgs.msg import Image
from tf2_msgs.msg import TFMessage
from cv_bridge import CvBridge # Package to convert between ROS and OpenCV Images
import cv2

def draw_axis(img, pitch, yaw, roll, center_x, center_y, size):
  y_flip = 1
  x_flip = 1
  x1 = x_flip * size * (math.cos(yaw) * math.cos(roll)) + center_x
  y1 = y_flip * size * (math.cos(pitch) * math.sin(roll) + math.cos(roll) * math.sin(pitch) * math.sin(yaw)) + center_y
  x2 = x_flip * size * (-math.cos(yaw) * math.sin(roll)) + center_x
  y2 = y_flip * size * (math.cos(pitch) * math.cos(roll) - math.sin(pitch) * math.sin(yaw) * math.sin(roll)) + center_y
  x3 = x_flip * size * (math.sin(yaw)) + center_x
  y3 = y_flip * size * (-math.cos(yaw) * math.sin(pitch)) + center_y

  # img , 시작, 끝 , BGR
  cv2.line(img, (int(center_x), int(center_y)), (int(x1),int(y1)),(0,0,255),3)
  cv2.line(img, (int(center_x), int(center_y)), (int(x2),int(y2)),(0,255,0),3)
  cv2.line(img, (int(center_x), int(center_y)), (int(x3),int(y3)),(255,0,0),3)

  return img

def euler_from_quaternion(x, y, z, w):
  t0 = +2.0 * (w * x + y * z)
  t1 = +1.0 - 2.0 * (x * x + y * y)
  x = math.atan2(t0, t1)

  t2 = +2.0 * (w * y - z * x)
  t2 = +1.0 if t2 > +1.0 else t2
  t2 = -1.0 if t2 < -1.0 else t2
  y = math.asin(t2)

  t3 = +2.0 * (w * z + x * y)
  t4 = +1.0 - 2.0 * (y * y + z * z)
  z = math.atan2(t3, t4)

  return x, y, z # in radians

class ImageSubscriber(Node):
  def __init__(self):
    super().__init__('my_image_viwer')
    self.image_sub = self.create_subscription(
      Image,
      'image',
      self.image_sub_callback,
      10)

    self.tf_sub = self.create_subscription(
      TFMessage,
      'tf',
      self.tf_sub_callback,
      10)

    self.frame_width = 1280
    self.frame_hegit = 720
    self.image_sub
    self.tf_sub
    self.br = CvBridge()
    self.tX = 0
    self.tY = 0
    self.tZ = 0
    self.rX = 0
    self.rY = 0
    self.rZ = 0
    self.rW = 0
  def tf_sub_callback(self, msg):
    translation = msg.transforms[0].transform.translation
    rotation = msg.transforms[0].transform.rotation
    # real Range : translation.x -0.21 ~ 0.32 , translation.y -0.22 ~ 0.06 , translation.z 0.2 ~ 0.8
    if translation.x != 0 and translation.y !=0 and translation.z != 0:
      self.tX = (translation.x + 0.2 )* 2 * self.frame_width 
      self.tY = (translation.y + 0.22 )* 3 *self.frame_hegit 
      self.tZ = 1 - translation.z
      self.rX = rotation.x
      self.rY = rotation.y
      self.rZ = rotation.z
      self.rW = rotation.w
      self.get_logger().info(f'{translation.x}, {translation.y}, {translation.z}')
      #self.get_logger().info(f'{self.tX}, {self.tY}, {self.tZ}')

  def image_sub_callback(self, msg):
    self.get_logger().debug('Receiving video frame')

    current_frame = self.br.imgmsg_to_cv2(msg)
    cv2.cvtColor(current_frame, cv2.COLOR_BGR2RGB, dst=current_frame)
    #cv2.flip(current_frame, 1)
    #self.frame_hegit, self.frame_width = current_frame.shape[:2]
    _x, _y, _z = euler_from_quaternion(self.rX, self.rY, self.rZ, self.rW)

    draw_axis(current_frame, _x, _y, _z , self.tX, self.tY, 100 * (self.tZ))
    cv2.imshow("camera", current_frame)
    cv2.waitKey(1)

def main(args=None):
  rclpy.init(args=args)

  image_subscriber = ImageSubscriber()
  rclpy.spin(image_subscriber)

  image_subscriber.destroy_node()

  rclpy.shutdown()

if __name__ == '__main__':
  main()