import rclpy
from rclpy.node import Node
from isaac_ros_apriltag_interfaces.msg import AprilTagDetectionArray
from std_msgs.msg import String

class ApriltagSubscriber(Node):

    def __init__(self):
        super().__init__('april_detecter')
        self.subscription = self.create_subscription(
            AprilTagDetectionArray, 
            'tag_detections', 
            self.sub_callback,
            10)
        
        self.publisher_ = self.create_publisher(String, 'april_info', 10)
        
    def detect_id(self, detections):
        for detect in detections:
            if detect.family =='tag36h11':
                return detect.id 
        return 'unkown'
    
    def sub_callback(self, submsg):
        april_id = self.detect_id(submsg.detections)
        self.get_logger().debug('april_id = "%s"' % april_id)
        pubmsg = String()
        pubmsg.data = 'april_id: %s' % april_id
        self.publisher_.publish(pubmsg)


def main(args=None):
    rclpy.init(args=args)

    april_subscriber = ApriltagSubscriber()

    rclpy.spin(april_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    april_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()


