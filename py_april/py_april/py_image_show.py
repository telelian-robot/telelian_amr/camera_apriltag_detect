import rclpy 
from rclpy.node import Node 
from sensor_msgs.msg import Image 
from std_msgs.msg import String
from tf2_msgs.msg import TFMessage
from cv_bridge import CvBridge # Package to convert between ROS and OpenCV Images
import cv2
 
class ImageSubscriber(Node):
  def __init__(self):
    super().__init__('my_image_viwer')
    self.image_sub = self.create_subscription(
      Image, 
      'image', 
      self.image_sub_callback, 
      10)
    
    self.text_sub = self.create_subscription(
      String, 
      'april_info', 
      self.text_sub_callback, 
      10)
    
    self.tf_sub = self.create_subscription(
      TFMessage, 
      'tf', 
      self.tf_sub_callback, 
      10)
    self.frame_heght = 720
    self.frame_width = 1280
    self.image_sub 
    self.text_sub
    self.april_info = " "  
    self.tX = 650
    self.tY = 300
    # Used to convert between ROS and OpenCV images
    self.br = CvBridge()

  def text_sub_callback(self, msg):
    self.get_logger().debug('I heard: "%s"' % msg.data)
    self.april_info = msg.data

  def tf_sub_callback(self, msg):
    translation = msg.transforms[0].transform.translation
    self.tX = translation.x * self.frame_width 
    self.tY = translation.y * self.frame_heght + 100
  def image_sub_callback(self, msg):
    self.get_logger().debug('Receiving video frame')
    self.get_logger().info(f'{self.tX} {self.tY}')

    current_frame = self.br.imgmsg_to_cv2(msg)
    cv2.cvtColor(current_frame, cv2.COLOR_BGR2RGB, dst=current_frame)
    font =  cv2.FONT_HERSHEY_PLAIN
    #current_frame = cv2.putText(current_frame, self.april_info, (int(self.tX), int(self.tY)), font, 4, (255,255,255), 4, cv2.LINE_AA)
    current_frame = cv2.putText(current_frame, "test", (int(self.tX), int(self.tY)), font, 1, (255,255,255), 1, cv2.LINE_AA)
    cv2.imshow("camera", current_frame)
    cv2.waitKey(1)
  
def main(args=None):
  rclpy.init(args=args)
  
  image_subscriber = ImageSubscriber()
  rclpy.spin(image_subscriber)

  image_subscriber.destroy_node()
  
  rclpy.shutdown()
  
if __name__ == '__main__':
  main()